<?php declare(strict_types=1);

namespace SubbkovOpenSource\Fibonacci;

class Fibonacci
{
    /**
     * @param int $number
     *
     * @return int
     */
    public function run(int $number): int
    {
        return (int) \round((((\sqrt(5) + 1) / 2) ** $number) / \sqrt(5));
    }
}